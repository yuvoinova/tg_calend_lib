package tg.calendar.lib;

import java.time.LocalDateTime;
import java.time.Month;
import org.springframework.stereotype.Component;

public class CalendarContext {
  private LocalDateTime timePoint;

  public CalendarContext() {
    timePoint = LocalDateTime.now();
  }

  public Month getMonth() {
    return timePoint.getMonth();
  }

  public int getYear() {
    return timePoint.getYear();
  }

  public void increaseMonth() {
    timePoint = timePoint.plusMonths(1);
  }

  public void decreaseMonth() {
    timePoint = timePoint.minusMonths(1);
  }
}
