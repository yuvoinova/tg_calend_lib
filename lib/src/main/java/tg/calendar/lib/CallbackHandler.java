package tg.calendar.lib;

import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@FunctionalInterface
public interface CallbackHandler {
  void execute(AbsSender absSender, User user, Chat chat, String callbackData);
}
