package tg.calendar.lib;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public class CalendarCallbackHandlingService {
  @Autowired private CalendarContextSupplier calendarContextSupplier;

  @Autowired
  @Qualifier("calendarDrawingCommand")
  private BotCommand botCommand;

  @Getter @Setter @Autowired private CallbackHandler dateSelectionCallbackHandler;

  public void execute(Update update, AbsSender absSender) {
    User user = update.getCallbackQuery().getFrom();
    Chat chat = update.getCallbackQuery().getMessage().getChat();
    String callbackData = update.getCallbackQuery().getData();
    if (callbackData.contains(CalendarCallbacksEnum.CALENDAR_SWITCHING_MONTH.toString())) {
      handleCallback(absSender, user, chat, callbackData, this::sendSwitchedCalendar);
    }
    if (callbackData.contains(CalendarCallbacksEnum.CALENDAR_DATE_SELECTION.toString())) {
      handleCallback(absSender, user, chat, callbackData, dateSelectionCallbackHandler);
    }
  }

  private static void handleCallback(
      AbsSender absSender,
      User user,
      Chat chat,
      String callbackData,
      CallbackHandler callbackHandler) {
    callbackHandler.execute(absSender, user, chat, callbackData);
  }

  private void sendSwitchedCalendar(
      AbsSender absSender, User user, Chat chat, String callbackData) {
    if (callbackData.contains(">>")) {
      calendarContextSupplier.getCalendarContext(user).increaseMonth();
    } else if (callbackData.contains("<<")) {
      calendarContextSupplier.getCalendarContext(user).decreaseMonth();
    }
    botCommand.execute(absSender, user, chat, null);
  }
}
