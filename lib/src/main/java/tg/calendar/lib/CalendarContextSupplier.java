package tg.calendar.lib;

import org.telegram.telegrambots.meta.api.objects.User;

public interface CalendarContextSupplier {
  public CalendarContext getCalendarContext(User user);
}
