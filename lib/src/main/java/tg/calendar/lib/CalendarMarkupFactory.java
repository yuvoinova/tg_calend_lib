package tg.calendar.lib;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;


public class CalendarMarkupFactory {
  @Autowired private CalendarContextSupplier calendarContextSupplier;
  private InlineKeyboardMarkup markup;

  public InlineKeyboardMarkup getMarkup(User user) {
    Month month = calendarContextSupplier.getCalendarContext(user).getMonth();
    int year = calendarContextSupplier.getCalendarContext(user).getYear();
    markup = new InlineKeyboardMarkup();
    List<List<InlineKeyboardButton>> calendarRowList = new ArrayList<>();
    Locale localeRu = new Locale("ru", "RU");
    calendarRowList.add(createMonthButtonRow(month, year, localeRu));
    calendarRowList.add(createDaysOfWeekButtonsRow(localeRu));
    calendarRowList.addAll(createDaysOfMonthButtonRow(month, year));
    calendarRowList.add(createMonthsSwitchingButtonsRow());
    markup.setKeyboard(calendarRowList);
    return markup;
  }

  private static List<InlineKeyboardButton> createMonthButtonRow(
      Month month, int year, Locale locale) {
    String displayName = month.getDisplayName(TextStyle.FULL_STANDALONE, locale) + " " + year;
    List<InlineKeyboardButton> MonthButtonRow = new ArrayList<>();
    MonthButtonRow.add(createButtonWithIgnoreCallback(displayName));
    return MonthButtonRow;
  }

  private static List<InlineKeyboardButton> createDaysOfWeekButtonsRow(Locale locale) {
    List<InlineKeyboardButton> daysOfWeekButtonsRow = new ArrayList<>();
    for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
      String displayName = dayOfWeek.getDisplayName(TextStyle.SHORT_STANDALONE, locale);
      daysOfWeekButtonsRow.add(createButtonWithIgnoreCallback(displayName));
    }
    return daysOfWeekButtonsRow;
  }

  private static List<List<InlineKeyboardButton>> createDaysOfMonthButtonRow(
      Month month, int year) {
    List<List<InlineKeyboardButton>> daysOfMonthRowList = new ArrayList<>();
    LocalDateTime currentDay = LocalDateTime.of(year, month, 1, 0, 0);
    while (!currentDay.getDayOfWeek().equals(DayOfWeek.MONDAY)) {
      currentDay = currentDay.minusDays(1);
    }
    while (!currentDay.getMonth().equals(month.plus(1))) {
      List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<>();
      for (int i = 0; i < 7; i++) {
        String displayName = String.valueOf(currentDay.getDayOfMonth());
        String currentDayStr = currentDay.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        String callbackData = CalendarCallbacksEnum.CALENDAR_DATE_SELECTION + " " + currentDayStr;
        keyboardButtonsRow.add(createButtonWithCallback(displayName, callbackData));
        currentDay = currentDay.plusDays(1);
      }
      daysOfMonthRowList.add(keyboardButtonsRow);
    }
    return daysOfMonthRowList;
  }

  private static List<InlineKeyboardButton> createMonthsSwitchingButtonsRow() {
    List<InlineKeyboardButton> monthsSwitchingButtonsRow = new ArrayList<>();
    monthsSwitchingButtonsRow.add(
        createButtonWithCallback("<<", CalendarCallbacksEnum.CALENDAR_SWITCHING_MONTH + " <<"));
    monthsSwitchingButtonsRow.add(
        createButtonWithCallback(">>", CalendarCallbacksEnum.CALENDAR_SWITCHING_MONTH + " >>"));
    return monthsSwitchingButtonsRow;
  }

  private static InlineKeyboardButton createButtonWithCallback(
      String displayName, String callbackData) {
    return InlineKeyboardButton.builder().text(displayName).callbackData(callbackData).build();
  }

  private static InlineKeyboardButton createButtonWithIgnoreCallback(String displayName) {
    return createButtonWithCallback(displayName, CalendarCallbacksEnum.CALENDAR_IGNORE.toString());
  }
}
