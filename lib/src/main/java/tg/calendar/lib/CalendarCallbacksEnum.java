package tg.calendar.lib;

public enum CalendarCallbacksEnum {
  CALENDAR_SWITCHING_MONTH,
  CALENDAR_DATE_SELECTION,
  CALENDAR_IGNORE
}
