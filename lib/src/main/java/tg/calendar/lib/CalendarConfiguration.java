package tg.calendar.lib;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CalendarConfiguration {
  @Bean
  CalendarCallbackHandlingService callbackHandlingService() {
    return new CalendarCallbackHandlingService();
  }

  @Bean
  CalendarContext calendarContext() {
    return new CalendarContext();
  }

  @Bean
  CalendarMarkupFactory calendarMarkupFactory() {
    return new CalendarMarkupFactory();
  }
}
